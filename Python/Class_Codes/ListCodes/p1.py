#List Creation Type 1
batsman = ["Rohit" , "Shubman" , "Virat" , "Shreyas" , "KL"];
print(batsman)
print(type(batsman))
print(type(batsman[0]))

batrun = ["Rohit" , 47 , "Shubman" , 80 , "Virat" , 117 , 7.9]
print(batrun)
print(type(batrun))
print(type(batrun[0]))

employee = [{10 , "Ashish", 1.5},{20 , "Kanha", 2.5} , {30 , "Rahul" , 3.5}]
print(employee)
print(type(employee))
print(type(employee[0]))

player = [{45 : "Rohit"} , {77 : "Shubman"} , {18 : "Virat"}]
print(player)
print(type(player))
print(type(player[0]))

jerNo = [(10, 20) ,(18,20) ,(10,30)]

print(jerNo)
print(type(jerNo))
print(type(jerNo[0]))


x = (10)
print(type(x))
x = [10]
print(type(x))
x = {10}
print(type(x))
x = {10:20}
print(type(x))
