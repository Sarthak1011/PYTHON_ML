player = ["rohit" , "shubman" , "virat" , "shreyas" , "KL"]

print(player[0])#rohit
print(player[1])#shubman
print(player[2])#virat

print(player[-3])#virat
print(player[-1])#KL
print(player[-2])#shreyas

print(player[::3])#['rohit' , shreyas]
print(player[4:2:2])#[]
print(player[4:2:-1])#[KL , Shreyas]

print(player[-1:-5:])#[]
print(player[-2:-4:-2])#['shreyas']
print(player[-1::-1])#['KL' , 'shreyas' , 'virat' , 'shubman' , 'rohit']
