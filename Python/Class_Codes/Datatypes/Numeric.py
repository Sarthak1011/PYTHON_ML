"""
                                         DATATYPES
____________________________________________|_________________________________________________
|                |               |                 |            |               |            |

1.Numeric Type   2.Boolean Type  3.Sequential Type  4.Set Type  5.Mapping Type  6.None Type  7.Byte Type
-int                -bool           -String           -type         -map            -none       - byes 
-float                              -list                                                       -bytearray
-complex                            -tuple
                                    -Range
"""

# numeric type

#int
empId = 20;

print(empId)

print(type(empId))

#float 
price = 75.50;

print(price)
print(type(price))

data = 45.1234567890123456789

print(data)
print(type(data))

#complex Number

complexData = 10 + 5j;

print(complexData)
print(type(complexData));

