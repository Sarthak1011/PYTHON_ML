
#string

friend1 = "Abhishek"

friend2 = 'Amar'

friend3 = '''Vishal'''

friend4 = """Akash"""


wing = 'A'
print(wing)
print(friend1)
print(friend2)
print(friend3)
print(friend4)

print(type(friend1))
print(type(friend2))
print(type(friend3))
print(type(friend4))
#List 

"""
List has Different types data 
dublicate data allow
we write String then list represnt as a ''
reprentation of list [ , , ]
list is mutable we cane change the data
"""
emplist = [63 , "Surya" , 54.3 ,'A',63]

print(emplist)
print(type(emplist))

emplist[2] = "Rahul"
print(emplist)

#tuple

"""
tuple has Differet type ofs data
tuple allow dublicate data
String represent tuple
representation of list ( , ,)
tuple is immutable we cannot change data

"""

emptuple = (45 , "Rohit" , 50.3 , 45 , 'A')
print(emptuple)
print(type(emptuple))

#emptuple[2] = "tilak"
#print(emptuple)

#range
x = range(1,31) # if it contain only on parameter then it start from 0

print(x)
print(type(x))

for i in x:
    print(i)

