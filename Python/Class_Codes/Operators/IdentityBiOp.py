x = 10
y = 20

print(x)  # 10
print(type(x)) # <class 'int'>
print(type(int)) # <class 'type'>

print(id(x)) # 12345678
print(id(y)) #7654321

print(id(int))

print(x is y)
print(x is not y)
