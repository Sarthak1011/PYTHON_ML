# Bitwise operator &  |  ^ <<  >> ~

x = 10
y = 20

# 10 -->01010
# 20 -->10100
print(x & y) # 0
print(x | y) #30
print(x ^ y) #30
print(x << 2)# 40
print(y >>3)#2

a  = 4 
print(~a) # -5
