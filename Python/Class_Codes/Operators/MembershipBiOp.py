# membership operator  in  not in

listData = [10,20,30,40,50]

x = 30
print(x in listData) # true
print(x not in listData) # false
