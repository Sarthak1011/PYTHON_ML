#logical and  logical or  logical not

x = True 
y = False

print(x and y) # False
print(x or y) # True
#print(x not y)   #SyntaxError: invalid syntax. Perhaps you forgot a comma?
print(not x) #False


a = 10  
b = 20

print(a and b)  # 20  in and operator consider the right value if 1st is F then return 1st but 1st is true then retrun 2nd value
print(a or b)   # 10  int or i operator if the left value is true then return 1 st val and left is false then restrun 2nd value
print(not b)    #False other than 0 is true 

p = 0
q = 20

print(p and q)  # 0
print(p or q)   # 20
print(not p)    #True





