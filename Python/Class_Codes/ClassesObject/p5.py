class Employee :

    def __init__(self , empId , empName):
        print("In Constructor");
        self.empId = empId
        self.empName = empName

    def empInfo(self):
        print(self.empId)
        print(self.empName)
        print(type(self))

emp1 = Employee(11 , "sarthak")
emp2 = Employee(10 , "Nikita")

emp1.empInfo()
emp2.empInfo()


