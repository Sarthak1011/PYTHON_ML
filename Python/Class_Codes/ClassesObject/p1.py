class Company(object) :
    x = 10
    def fun(self):
        y = 20
        print(y) 
obj = Company()
print(type(Company))
print(type(print))
print(type(obj))
print(obj.fun().y)
print(obj.x)

'''
class Client(Company):
    print(x)
obj = Client();    
print(type(Client))
print(type(obj))
print(obj)
print(Company)
'''
