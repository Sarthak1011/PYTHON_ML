class Player :

    country = "India"
    def __init__(self, jerNo , pName):

        self.jerNo = jerNo
        self.pName = pName
    
    def playerInfo(self):
        print(self.jerNo)
        print(self.pName)
        print(self.country)
        print(Player.country)
    

p1 = Player(45 ,"Rohit")
p2 = Player(63 , "SKY")

p1.playerInfo()# 45 Rohit India India 
p2.playerInfo()# 63 SKY India India

p1.country = "Bharat"

p1.playerInfo() # 45 Rohit Bharat India
p2.playerInfo() # 63 Sky India India

Player.country = "Australia"

p1.playerInfo() # 45 Rohit Bharat Australia
p2.playerInfo() #     63 Sky Australia Australia



