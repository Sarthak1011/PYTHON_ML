class Client{
        void merge(int arr[],int start,int mid,int end){

                int size1= mid-start+1;
                int size2= end-mid;

                int arr1[]=new int[size1];
                int arr2[]=new int[size2];

                for(int i=start;i<arr1.length;i++){
                        arr1[i]=arr[i];
                }
                for(int i=start;i<end;i++){
                        arr2[i]=arr[mid+i+1];
                }

                int i=0;
                int j=0;
                int k=start;

                while(i < arr1.length && j < arr2.length){

                        if(arr1[i] < arr2[j]){
                                arr[k]=arr1[i];
                                i++;
                        }else{
                                arr[k]=arr2[j];
                                j++;
                        }
                        k++;
                }
                while(i < arr1.length){
                        arr[k]=arr1[i];
                        i++;
                        k++;
                }
                while(j < arr2.length){
                        arr[k]=arr2[j];
                        j++;
                        k++;
                }
        }
        void mergeSort(int arr[],int start,int end){

                while(start > end){
                        return;
                }

                int mid = start + (end-start)/2;

                mergeSort(arr,start,mid);
                mergeSort(arr,mid+1,end);
                merge(arr,start,mid,mid);
        }

        public static void main(String[] args){

                int arr[]=new int[]{1,4,6,3,7,2,5};

                Client obj=new Client();

                obj.mergeSort(arr,0,arr.length-1);

                System.out.println("Sorted Array Becomes");

                for(int i=0;i<arr.length;i++){
                        System.out.print(arr[i]+"  ");
                }
                System.out.println();
        }
}
