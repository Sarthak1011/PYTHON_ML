def Outer():

    print("In Outer")

    def Inner():
        print("In Inner")
    Inner()    

print("Start")
Outer()
print("End")
