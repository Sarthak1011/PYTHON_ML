'''
1  2  3  4   5
6  7  8  9  10
12 18 20 21 24
27 30 36 40 42
45 48 50 54 60

'''

row = int(input("Enter the rows:"))
num = 1
temp = num
for i in range(row):
    for j in range(row):
        summ = 0
        num = temp
        while(num>= 0):
            summ = summ + num % 10
            num = num / 10
    
        if(temp % summ == 0):
            print(temp,end = "\t")
        temp = temp +1
    print()
        
