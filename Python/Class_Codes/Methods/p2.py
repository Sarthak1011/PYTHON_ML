class Demo :

    x = 10

    def __init__(self):
        self.y = 20
        print("In Constructor",self)
    
    @classmethod
    def fun(cls):
        print("In Class method",cls)
       #print(cls.y) error
        print(cls.x)
      #  print(self.y)
      #  print(self.x)


obj = Demo()
obj.fun()
Demo.fun()

