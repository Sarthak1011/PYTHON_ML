class Demo :

    x = 10
    def __init__(self):
        self.y = 20
        print("In constructor")

    def fun2(self):
        print(self)
        print(type(self.fun))
        print(type(self.fun1))

    @staticmethod
    def fun():
        print()

    @classmethod
    def fun1(cls):
        print()

obj = Demo()
obj.fun1()
obj.fun2()
print(staticmethod)
print(classmethod)
