def DecorFun(func):

    def wrapper():
        print("Start")
        func()
        print("end")
    return wrapper

#@DecorFun
def normalFun():
    print("Hello")

normalFun = DecorFun(normalFun)
normalFun()
